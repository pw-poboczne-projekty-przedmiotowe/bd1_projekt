--Trig dla autora
/
    create sequence ID_gen_autor
        increment by 1
        start with 1
        minvalue 1
        maxvalue 999
        cycle
        cache 2;
/
    create or replace trigger trig_id_gen_autor
    before insert on autorzy
    for each row
    begin
      :new.id_autora := ID_gen_autor.nextval;
    end;
--trig dla gatunku
/
    create sequence ID_gen_gatunki
        increment by 1
        start with 1
        minvalue 1
        maxvalue 999
        cycle
        cache 2;
/
    create or replace trigger trig_ID_gen_gatunki
    before insert on gatunki
    for each row
    begin
      :new.id_gatunku := ID_gen_gatunki.nextval;
    end;
--Trig dla historii
/
    create sequence ID_gen_historia
        increment by 1
        start with 1
        minvalue 1
        maxvalue 9999
        cycle
        cache 2;
/
    create or replace trigger trig_ID_gen_historia
    before insert on historia
    for each row
    begin
      :new.id_wpisu := ID_gen_historia.nextval;
    end;
--trig dla karta_biblioteczna
/
    create sequence ID_gen_karta_bibl
        increment by 1
        start with 1
        minvalue 1
        maxvalue 999
        cycle
        cache 2;
/
    create or replace trigger trig_ID_gen_karta_bibl
    before insert on karta_biblioteczna
    for each row
    begin
      :new.id_karty := ID_gen_karta_bibl.nextval;
    end;
--trig dla ksiazki
/
    create sequence ID_gen_ksiazka
    increment by 1
        start with 1
        minvalue 1
        maxvalue 999
        cycle
        cache 2;
/
    create or replace trigger trig_ID_gen_ksiazka
    before insert on ksiazki
    for each row
    begin
      :new.id_ksiazki := ID_gen_ksiazka.nextval;
    end;  
--trig dla osoby
/
    create sequence ID_gen_osoba
    increment by 1
        start with 1
        minvalue 1
        maxvalue 9999
        cycle
        cache 2;
/
    create or replace trigger trig_ID_gen_osoba
    before insert on osoba
    for each row
    begin
      :new.id_osoby := ID_gen_osoba.nextval;
    end;  