create or replace procedure dodaj_ksiazke
(
tytul_ksiazki VARCHAR2,
imie_autora VARCHAR2,
nazwisko_autora   VARCHAR2,
rok_wydania NUMBER,
gatunek_nazwa VARCHAR2
)
as
autor autorzy%rowtype;
gatunek gatunki%rowtype;
numer number;
begin
select count(*) into numer from autorzy 
where imie=imie_autora and nazwisko=nazwisko_autora;
if (numer = 0)
then
dbms_output.put_line('Brak autora, tworzenie nowego');
insert into autorzy values (null,imie_autora, nazwisko_autora);
end if;
select * into autor from autorzy where imie=imie_autora and nazwisko=nazwisko_autora;

select count(*) into numer from gatunki where gatunek_nazwa=nazwa;
if (numer = 0)
then
dbms_output.put_line('Brak danego gatunku, tworzenie nowego');
insert into gatunki values (null,gatunek_nazwa);
end if;

select * into gatunek from gatunki where gatunek_nazwa=nazwa;

insert into ksiazki values (null,tytul_ksiazki,rok_wydania,autor.id_autora,gatunek.id_gatunku);

end dodaj_ksiazke;
/

create or replace procedure wypozycz_ksiazke
(
    id_ksiazki_wypozyczanej NUMBER,
    id_karty NUMBER
)
as
numer number;
begin
select count(*) into numer from historia 
where ksiazki_id_ksiazki=id_ksiazki_wypozyczanej and data_oddania is null;
if (numer = 0)
then
insert into historia values (null,sysdate,null,id_ksiazki_wypozyczanej,id_karty);
else
dbms_output.put_line('Ksiazka jest juz wypoczyczona');
end if;

end wypozycz_ksiazke;
/

create or replace procedure zwroc_ksiazke
(
    id_ksiazki_zwracanej NUMBER
)
as
wpis historia%rowtype;
dni_trzymania number;
max_okres_trzymania number := 30;
oplata_dzienna number := 0.30;
naliczona_oplata number := 0;
begin
select * into wpis from historia 
where ksiazki_id_ksiazki=id_ksiazki_zwracanej and data_oddania is null; 
dni_trzymania := sysdate-wpis.data_wypozyczenia;
if (dni_trzymania>max_okres_trzymania)
then
naliczona_oplata := (dni_trzymania-max_okres_trzymania)*oplata_dzienna;
dbms_output.put_line('Przetrzymanie ksiazki, oplata:' || naliczona_oplata);
update karta_biblioteczna set oplata = oplata + naliczona_oplata 
where id_karty=wpis.karta_biblioteczna_id_karty;
end if;

update historia set data_oddania = sysdate where id_wpisu=wpis.id_wpisu;
end zwroc_ksiazke;
/

create or replace function policz_sume_zaleglych_oplat return number

as 
suma_zaleglosci number := 0;
cursor c is select oplata from karta_biblioteczna where oplata>0;
begin
for rekord in c
loop
suma_zaleglosci := suma_zaleglosci + rekord.oplata;
end loop;
dbms_output.put_line('Suma oplat:' || suma_zaleglosci);
return suma_zaleglosci;
end policz_sume_zaleglych_oplat;
/


create or replace function ile_jest_wypozyczonych_ksiazek return number
as
wypozyczono number := 0;
begin
select count(*) into wypozyczono from historia where data_oddania is null;
return wypozyczono;
end;


/
create or replace procedure ranking_ksiazek
(
    poczatek date,
    koniec date
)
as
cursor wypozyczenia is
SELECT * FROM 
(select ksiazki_id_ksiazki, count(*) as cnt from historia 
where data_wypozyczenia>poczatek and data_wypozyczenia<koniec
group by historia.ksiazki_id_ksiazki 
order by count (*) DESC) 
where rownum <=10;

ksiazka ksiazki%rowtype;
autor autorzy%rowtype;
begin
dbms_output.put_line('Ranking:');

for wiersz in wypozyczenia
loop
SELECT * into ksiazka from ksiazki where id_ksiazki=wiersz.ksiazki_id_ksiazki;
select * into autor from autorzy where id_autora=ksiazka.autorzy_id_autora;
dbms_output.put_line(ksiazka.tytul || ' ' || autor.imie || ' ' 
||autor.nazwisko || ' liczba wypozyczeń: ' || wiersz.cnt);
end loop;

end ranking_ksiazek;
/

