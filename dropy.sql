--tabel
drop table autorzy CASCADE CONSTRAINTS;
drop table gatunki CASCADE CONSTRAINTS;
drop table historia CASCADE CONSTRAINTS;
drop table karta_biblioteczna CASCADE CONSTRAINTS;
drop table ksiazki CASCADE CONSTRAINTS;
drop table osoba CASCADE CONSTRAINTS;

--triggery i sequence
drop sequence ID_gen_autor;
drop sequence ID_gen_gatunki;
drop sequence ID_gen_historia;
drop sequence ID_gen_karta_bibl;
drop sequence ID_gen_ksiazka;
drop sequence ID_gen_osoba;

drop trigger trig_ID_gen_autor;
drop trigger trig_ID_gen_gatunki;
drop trigger trig_ID_gen_historia;
drop trigger trig_ID_gen_karta_bibl;
drop trigger trig_ID_gen_ksiazka;
drop trigger trig_ID_gen_osoba;

--



