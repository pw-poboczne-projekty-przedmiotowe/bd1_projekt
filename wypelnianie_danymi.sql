

-- tutaj jednocześnie wypełniamy tabele autorów, gatunków, książek oraz testujemy poprawność dodawania książek
begin

dodaj_ksiazke('Tytuł1','Jan','Kowalski',2002,'thriller');
dodaj_ksiazke('Tytuł2','Jan','Kowalski',2003,'thriller');
dodaj_ksiazke('Kroniki','Gall','Anonim',1400,'historia');
dodaj_ksiazke('Harry Potter Tom1','Joanne','Rowling',2000,'fantasy');
dodaj_ksiazke('Harry Potter Tom2','Joanne','Rowling',2003,'fantasy');
dodaj_ksiazke('Harry Potter Tom3','Joanne','Rowling',2006,'fantasy');
dodaj_ksiazke('Harry Potter Tom4','Joanne','Rowling',2010,'fantasy');
dodaj_ksiazke('Harry Potter Tom5','Joanne','Rowling',2012,'fantasy');
dodaj_ksiazke('Harry Potter Tom6','Joanne','Rowling',2015,'fantasy');
dodaj_ksiazke('Harry Potter Tom7','Joanne','Rowling',2016,'fantasy');




end;
/
begin--osoby
insert into osoba values (null,'John','Faviet',null);
insert into osoba values (null,'Ismael','Chen',null);
insert into osoba values (null,'JoseManuel','Sciarra',null);
insert into osoba values (null,'Luis','Urman',null);
insert into osoba values (null,'Luis','Popp',null);
insert into osoba values (null,'Den','Raphaely',null);
insert into osoba values (null,'Alexander','Khoo',null);
insert into osoba values (null,'Shelli','Baida',null);
insert into osoba values (null,'Sigal','Tobias',null);
insert into osoba values (null,'Guy','Himuro',null);
insert into osoba values (null,'Karen','Himuro',null);
insert into osoba values (null,'Matthew','Colmenares',123456789);
insert into osoba values (null,'Adam','Weiss',null);
insert into osoba values (null,'Adam','Fripp',null);
insert into osoba values (null,'Kevin','Kaufling',null);
insert into osoba values (null,'Julia','Vollman',null);
insert into osoba values (null,'Irene','Janowski',null);
insert into osoba values (null,'Steven','Mourgos',null);
insert into osoba values (null,'Steven','Janowski',null);
insert into osoba values (null,'Mozhe','Nayer',null);
insert into osoba values (null,'James','Mikkilineni',null);
insert into osoba values (null,'Jason','Landry',null);
insert into osoba values (null,'Michael','Markle',null);
insert into osoba values (null,'Ki','Bissot',null);
insert into osoba values (null,'Renske','Atkinson',null);
insert into osoba values (null,'Stephen','Atkinson',null);
insert into osoba values (null,'John','Marlow',994234242);
insert into osoba values (null,'Joshua','Olson',null);
insert into osoba values (null,'Trenna','Mallin',null);
insert into osoba values (null,'Randall','Rogers',null);
insert into osoba values (null,'Peter','Gee',null);
insert into osoba values (null,'John','Philtanker',null);
insert into osoba values (null,'Peter','Gee',987654321);



insert into karta_biblioteczna values (null,0,1);
insert into karta_biblioteczna values (null,0,2);
insert into karta_biblioteczna values (null,0,3);
insert into karta_biblioteczna values (null,0,4);
insert into karta_biblioteczna values (null,0,5);
insert into karta_biblioteczna values (null,0,6);
insert into karta_biblioteczna values (null,0,7);
insert into karta_biblioteczna values (null,0,8);
insert into karta_biblioteczna values (null,0,9);
insert into karta_biblioteczna values (null,0,10);
insert into karta_biblioteczna values (null,0,11);
insert into karta_biblioteczna values (null,0,12);
insert into karta_biblioteczna values (null,0,13);
insert into karta_biblioteczna values (null,0,14);
insert into karta_biblioteczna values (null,0,15);
insert into karta_biblioteczna values (null,0,16);
insert into karta_biblioteczna values (null,0,17);
insert into karta_biblioteczna values (null,0,18);
insert into karta_biblioteczna values (null,0,19);
insert into karta_biblioteczna values (null,0,20);
insert into karta_biblioteczna values (null,0,21);
insert into karta_biblioteczna values (null,0,22);
insert into karta_biblioteczna values (null,0,23);
insert into karta_biblioteczna values (null,0,24);
insert into karta_biblioteczna values (null,0,25);
insert into karta_biblioteczna values (null,0,26);
insert into karta_biblioteczna values (null,0,27);
insert into karta_biblioteczna values (null,0,28);
insert into karta_biblioteczna values (null,0,29);
insert into karta_biblioteczna values (null,0,30);
insert into karta_biblioteczna values (null,0,31);
insert into karta_biblioteczna values (null,0,32);
insert into karta_biblioteczna values (null,0,33);

end;

/
--stare wpisy z wypozyczonymi juz ksiazkami
begin
--                         (id,   data wyp,data oddania,id_ksiazki, id_karty,)
insert into historia values(null,'20/01/02','20/01/12',1,1); 
insert into historia values(null,'20/01/04','20/01/12',2,2);
insert into historia values(null,'20/01/05','20/01/12',3,3);
insert into historia values(null,'20/01/04','20/01/12',4,1);
insert into historia values(null,'20/01/09','20/01/14',5,1);
insert into historia values(null,'20/01/21','20/01/28',6,2);
insert into historia values(null,'20/02/04','20/02/17',7,3);
insert into historia values(null,'20/02/04','20/02/19',8,5);
insert into historia values(null,'20/02/04','20/02/19',4,1);
insert into historia values(null,'20/02/04','20/02/23',6,2);
insert into historia values(null,'20/02/06','20/02/23',3,3);
insert into historia values(null,'20/02/06','20/02/24',2,6);
insert into historia values(null,'20/02/06','20/02/25',1,4);
insert into historia values(null,'20/02/06','20/02/25',5,1);
insert into historia values(null,'20/03/02','20/03/12',1,1); 
insert into historia values(null,'20/03/04','20/03/12',2,2);
insert into historia values(null,'20/03/05','20/03/12',3,3);
insert into historia values(null,'20/03/04','20/03/12',4,1);
insert into historia values(null,'20/03/09','20/03/14',5,1);
insert into historia values(null,'20/03/21','20/03/28',6,2);
insert into historia values(null,'20/04/04','20/04/17',7,1);
insert into historia values(null,'20/04/04','20/04/09',8,3);
insert into historia values(null,'20/04/04','20/04/19',4,1);
insert into historia values(null,'20/04/04','20/04/21',6,8);
insert into historia values(null,'20/04/06','20/04/29',3,5);
insert into historia values(null,'20/04/06','20/04/29',2,9);
insert into historia values(null,'20/04/06','20/04/25',1,1);
insert into historia values(null,'20/04/06','20/04/25',2,1);





end;
/