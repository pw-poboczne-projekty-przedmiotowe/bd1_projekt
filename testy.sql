--testy procedur i funkcji
begin
--dodajemy książkę
dodaj_ksiazke('Niekończąca się opowieść','Autor','Autorski',2001,'fantasy');
end;
/
--wypisujemy tą książkę
select * from ksiazki 
where tytul='Niekończąca się opowieść';
/
begin

wypozycz_ksiazke(8,2);
--próbujemy wypożyczyć ją drugi raz drugiemu czytelnikowi
wypozycz_ksiazke(2,2); --wopinno nam wypisać informację o tym, że jest już wypożyczona
end;
/
--tu dla pewności sprawdzamy, czy nadal jest tylko jedno wypożyczenie tej książki
select * from historia where ksiazki_id_ksiazki=2 and data_oddania is null;
/
--zwracamy tą książkę
begin zwroc_ksiazke(2); end;
/
--sprawdzamy, czy nadal jest
select * from historia where ksiazki_id_ksiazki=2 and data_oddania is null;


/
--zwracanie książki, które naliczy opłatę

insert into historia values(null,'20/04/05',null,3,4);

select oplata from karta_biblioteczna where id_karty=4;
/
--powina zostać wyliczona opłata i wypisana jej wartość
begin zwroc_ksiazke(3); end;
/
--powinna się pojawić opłata na tej karcie
select oplata from karta_biblioteczna where id_karty=4;



/
--proste testy funkcji wypisujących
begin
dbms_output.put_line('Suma zaległych opłat : '||policz_sume_zaleglych_oplat);
dbms_output.put_line('Liczba obecnie wypozyczonych książek: '||ile_jest_wypozyczonych_ksiazek);
ranking_ksiazek('20/01/02',sysdate);
end;

/

--nietrywialne wywołania

--lista osób, które nie oddały jeszcze książki
select imie, nazwisko, telefon, oplata, id_karty 
from karta_biblioteczna inner join osoba on( id_osoby=osoba_id_osoby) 
where id_karty = any(
select ksiazki_id_ksiazki from historia 
where data_oddania is null GROUP by ksiazki_id_ksiazki);

--lista autorów z liczbą ich książek
SELECT
id_autora,imie,nazwisko,count(*) as liczba_ksiazek
FROM autorzy inner join  ksiazki on (id_autora=autorzy_id_autora) 
group by id_autora,imie,nazwisko
order by count(*) desc;

--najaktywniejsi czytelnicy (licząc od początku lutego)
select id_karty,imie, nazwisko,count(*) as ile_wypozyczen 
from karta_biblioteczna 
inner join historia
on(id_karty=karta_biblioteczna_id_karty)
inner join osoba
on(osoba_id_osoby=id_osoby)
where data_wypozyczenia > '20/02/01'
group by id_karty,imie, nazwisko
order by count(*) desc;

--najpopularniejsze gatunki
select nazwa, count(*) from gatunki 
inner join ksiazki 
on (gatunki_id_gatunku=id_gatunku)
inner join historia
on (ksiazki_id_ksiazki=id_ksiazki)
group by gatunki.nazwa
order by count(*) desc;

--10 ostatnich wypożyczeń danego właściciela karty
select * from 
(select tytul, data_wypozyczenia, data_oddania from osoba 
inner join karta_biblioteczna
on (osoba_id_osoby=id_osoby)
inner join historia
on (karta_biblioteczna_id_karty=id_karty)
inner join ksiazki
on (id_ksiazki=ksiazki_id_ksiazki)
where id_osoby=1
order by data_wypozyczenia desc)
where rownum <=10
;

--lista osób bez kart (będzie tutaj tylkos uper Marian)
insert into osoba values(null,'Super','Marian',986543134);

select * from osoba left outer join
karta_biblioteczna
on (osoba_id_osoby=id_osoby)
where id_karty is null;



