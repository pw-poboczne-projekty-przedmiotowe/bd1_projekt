CREATE TABLE autorzy (
    id_autora  NUMBER(3) NOT NULL,
    imie       VARCHAR2(20 BYTE) NOT NULL,
    nazwisko   VARCHAR2(20 BYTE) NOT NULL
);

ALTER TABLE autorzy ADD CONSTRAINT autorzy_pk PRIMARY KEY ( id_autora );

CREATE TABLE gatunki (
    id_gatunku  NUMBER(3) NOT NULL,
    nazwa       VARCHAR2(15 BYTE) NOT NULL
);

ALTER TABLE gatunki ADD CONSTRAINT gatunki_pk PRIMARY KEY ( id_gatunku );

CREATE TABLE historia (
    id_wpisu                     NUMBER(4) NOT NULL,
    data_wypozyczenia            DATE NOT NULL,
    data_oddania                 DATE,
    ksiazki_id_ksiazki           NUMBER(3) NOT NULL,
    karta_biblioteczna_id_karty  NUMBER(3) NOT NULL
);

ALTER TABLE historia ADD CONSTRAINT historia_pk PRIMARY KEY ( id_wpisu );

CREATE TABLE karta_biblioteczna (
    id_karty        NUMBER(3) NOT NULL,
    oplata          NUMBER(4, 2),
    osoba_id_osoby  NUMBER NOT NULL
);

ALTER TABLE karta_biblioteczna ADD CONSTRAINT karta_biblioteczna_pk PRIMARY KEY ( id_karty );

CREATE TABLE ksiazki (
    id_ksiazki          NUMBER(3) NOT NULL,
    tytul               VARCHAR2(30 BYTE) NOT NULL,
    rok                 NUMBER,
    autorzy_id_autora   NUMBER(3) NOT NULL,
    gatunki_id_gatunku  NUMBER(3) NOT NULL
);

ALTER TABLE ksiazki ADD CONSTRAINT ksiazki_pk PRIMARY KEY ( id_ksiazki );

CREATE TABLE osoba (
    id_osoby  NUMBER NOT NULL,
    imie      VARCHAR2(20 BYTE) NOT NULL,
    nazwisko  VARCHAR2(20 BYTE) NOT NULL,
    telefon   NUMBER
);

ALTER TABLE osoba ADD CONSTRAINT osoba_pk PRIMARY KEY ( id_osoby );

ALTER TABLE historia
    ADD CONSTRAINT historia_karta_biblioteczna_fk FOREIGN KEY ( karta_biblioteczna_id_karty )
        REFERENCES karta_biblioteczna ( id_karty );

ALTER TABLE historia
    ADD CONSTRAINT historia_ksiazki_fk FOREIGN KEY ( ksiazki_id_ksiazki )
        REFERENCES ksiazki ( id_ksiazki );

ALTER TABLE karta_biblioteczna
    ADD CONSTRAINT karta_biblioteczna_osoba_fk FOREIGN KEY ( osoba_id_osoby )
        REFERENCES osoba ( id_osoby );

ALTER TABLE ksiazki
    ADD CONSTRAINT ksiazki_autorzy_fk FOREIGN KEY ( autorzy_id_autora )
        REFERENCES autorzy ( id_autora );

ALTER TABLE ksiazki
    ADD CONSTRAINT ksiazki_gatunki_fk FOREIGN KEY ( gatunki_id_gatunku )
        REFERENCES gatunki ( id_gatunku );
